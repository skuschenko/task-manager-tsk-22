package com.tsc.skuschenko.tm.api;

import com.tsc.skuschenko.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity);

    void clear();

    List<E> findAll();

    E findById(String id);

    void remove(E entity);

    E removeById(String id);

}
