package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.model.User;

public interface IAuthService {

    void checkRoles(final Role... roles);

    User getUser();

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

}
