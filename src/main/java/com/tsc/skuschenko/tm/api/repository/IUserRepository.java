package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByEmail(String email);

    User findByLogin(String login);

    User removeByLogin(String login);

}
