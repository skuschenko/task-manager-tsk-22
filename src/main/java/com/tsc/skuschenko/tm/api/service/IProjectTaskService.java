package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskByProject(String userId, String projectId, String taskId);

    void clearProjects(String userId);

    Project deleteProjectById(String userId, String projectId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

}
