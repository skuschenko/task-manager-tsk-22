package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    Task add(String userId, String name, String description);

}
