package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByEmail(String email);

    User findByLogin(String login);

    boolean isEmailExist(String login);

    boolean isLoginExist(String login);

    User lockUserByLogin(String login);

    User removeByLogin(String login);

    User setPassword(String userId, String password);

    User unlockUserByLogin(String login);

    User updateUser(
            String userId, String firstName, String lastName, String middleName
    );

}
