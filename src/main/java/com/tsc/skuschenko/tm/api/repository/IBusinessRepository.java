package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity>
        extends IRepository<E> {

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findOneById(String userId, String id);

    E findOneByIndex(String userId, Integer index);

    E findOneByName(String userId, String name);

    E removeOneById(String userId, String id);

    E removeOneByIndex(String userId, Integer index);

    E removeOneByName(String userId, String name);

}
