package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

}
