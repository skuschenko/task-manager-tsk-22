package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand abstractCommand);

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandArgs();

    AbstractCommand getCommandByArg(String name);

    AbstractCommand getCommandByName(String name);

    Collection<String> getCommandNames();

    Collection<AbstractCommand> getCommands();

}