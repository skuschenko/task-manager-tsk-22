package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.api.service.*;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.command.project.*;
import com.tsc.skuschenko.tm.command.system.*;
import com.tsc.skuschenko.tm.command.task.*;
import com.tsc.skuschenko.tm.command.user.*;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.system.UnknownArgumentException;
import com.tsc.skuschenko.tm.exception.system.UnknownCommandException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.CommandRepository;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import com.tsc.skuschenko.tm.repository.UserRepository;
import com.tsc.skuschenko.tm.service.*;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.Optional;

public class Bootstrap implements IServiceLocator {

    private final String OPERATION_FAIL = "fail";

    private final String OPERATION_OK = "ok";

    private final ICommandRepository commandRepository =
            new CommandRepository();

    private final ICommandService commandService =
            new CommandService(commandRepository);

    private final ILogService logService = new LogService();

    private final IProjectRepository projectRepository =
            new ProjectRepository();

    private final IProjectService projectService =
            new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(taskRepository, projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ProjectViewByIdCommand());
        registry(new ProjectViewByIndexCommand());
        registry(new ProjectViewByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectDeleteByIdCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskListCommand());
        registry(new TaskViewByIdCommand());
        registry(new TaskViewByIndexCommand());
        registry(new TaskViewByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskFindAllByProjectIdCommand());
        registry(new TaskBindByProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new AboutShowCommand());
        registry(new AllArgumentsShowCommand());
        registry(new AllCommandsShowCommand());
        registry(new HelpShowCommand());
        registry(new ProgramExitCommand());
        registry(new SystemInfoShowCommand());
        registry(new VersionShowCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLogoutCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUnlockByLoginCommand());
        registry(new UserLockByLoginCommand());
    }

    private void createDefaultProjectAndTask(
            final User user, final String name, String description
    ) {
        final Project project =
                projectService.add(user.getId(), name, description);
        project.setUserId(user.getId());
        final Task task = taskService.add(user.getId(), name, description);
        task.setProjectId(project.getId());
        task.setUserId(user.getId());
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    private void initUsers() {
        User user = userService.create("test", "test", "text@test.ru");
        createDefaultProjectAndTask(user, "Test", "Test");
        user = userService.create("admin", "admin", Role.ADMIN);
        createDefaultProjectAndTask(user, "Admin", "Admin");
    }

    public void parseArg(final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        final AbstractCommand abstractCommand
                = commandService.getCommandByArg(arg);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownArgumentException(arg));
        abstractCommand.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (!Optional.ofNullable(args).filter(item -> item.length > 1)
                .isPresent()) {
            return false;
        }
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(final String command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        final AbstractCommand abstractCommand
                = commandService.getCommandByName(command);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownCommandException(command));
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        logService.debug("TEST");
        logService.info("***Welcome to task manager***");
        initUsers();
        if (parseArgs(args)) exit();
        while (true) {
            try {
                System.out.print("Enter command:");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("[" + OPERATION_OK.toUpperCase() + "]");
            } catch (Exception e) {
                logService.error(e);
                System.out.println("[" + OPERATION_FAIL.toUpperCase() + "]");
            }
        }
    }

}
