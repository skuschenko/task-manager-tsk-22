package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "update project by index";

    private final String NAME = "project-update-by-index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("index");
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        final IProjectService projectService
                = serviceLocator.getProjectService();
        Project project = projectService.findOneByIndex(userId, valueIndex);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showParameterInfo("name");
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo("description");
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneByIndex(
                userId, valueIndex, valueName, valueDescription
        );
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @Override
    public String name() {
        return NAME;
    }

}
