package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "update task by id";

    private final String NAME = "task-update-by-id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("id");
        final String valueId = TerminalUtil.nextLine();
        final ITaskService taskService
                = serviceLocator.getTaskService();
        Task task = taskService.findOneById(userId, valueId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showParameterInfo("name");
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo("description");
        final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneById(
                userId, valueId, valueName, valueDescription
        );
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @Override
    public String name() {
        return NAME;
    }

}