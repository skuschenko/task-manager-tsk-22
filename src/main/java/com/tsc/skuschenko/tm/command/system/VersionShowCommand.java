package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

public class VersionShowCommand extends AbstractCommand {

    private final String ARGUMENT = "-v";

    private final String DESCRIPTION = "version";

    private final String NAME = "version";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        System.out.println("1.0.0");
    }

    @Override
    public String name() {
        return NAME;
    }

}