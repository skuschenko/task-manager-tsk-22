package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "change password of current user";

    private final String NAME = "change-user-password";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final String userId = serviceLocator.getAuthService().getUserId();
        showParameterInfo("new password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @Override
    public String name() {
        return NAME;
    }

}
