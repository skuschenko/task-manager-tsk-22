package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

public class AboutShowCommand extends AbstractCommand {

    private final String ARGUMENT = "-a";

    private final String DESCRIPTION = "about";

    private final String NAME = "about";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        System.out.println("AUTHOR: Semyon Kuschenko");
        System.out.println("EMAIL: skushchenko@tsconsulting.com");
    }

    @Override
    public String name() {
        return NAME;
    }

}
