package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;

public class TaskClearCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "clear all tasks";

    private final String NAME = "task-clear";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.clear(userId);
    }

    @Override
    public String name() {
        return NAME;
    }

}
