package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "lock user by login";

    private final String NAME = "lock-user-by-login";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
