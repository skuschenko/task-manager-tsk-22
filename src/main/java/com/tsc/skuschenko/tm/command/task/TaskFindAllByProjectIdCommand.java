package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.List;
import java.util.Optional;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "find all task by project id";

    private final String NAME = "find-all-task-by-project-id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("project id");
        final String value = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        final List<Task> tasks
                = projectTaskService.findAllTaskByProjectId(userId, value);
        Optional.ofNullable(tasks).filter(item -> item.size() != 0).get()
                .forEach(item -> showTask(item));
    }

    @Override
    public String name() {
        return NAME;
    }

}
