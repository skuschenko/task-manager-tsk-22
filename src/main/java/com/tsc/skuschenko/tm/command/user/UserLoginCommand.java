package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "user login";

    private final String NAME = "login";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public String name() {
        return NAME;
    }

}
