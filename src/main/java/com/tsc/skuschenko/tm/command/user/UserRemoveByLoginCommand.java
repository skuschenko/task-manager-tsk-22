package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "remove user by login";

    private final String NAME = "remove-user-by-login";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(password);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
