package com.tsc.skuschenko.tm.command.user;

public class UserLogoutCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "logout current user";

    private final String NAME = "logout";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String name() {
        return NAME;
    }

}
