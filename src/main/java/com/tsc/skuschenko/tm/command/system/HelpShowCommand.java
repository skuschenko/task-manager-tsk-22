package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class HelpShowCommand extends AbstractCommand {

    private final String ARGUMENT = "-h";

    private final String DESCRIPTION = "help";

    private final String NAME = "help";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final Collection<AbstractCommand> commands =
                serviceLocator.getCommandService().getCommands();
        commands.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(item -> {
                    String result = "";
                    if (Optional.ofNullable(item.name()).isPresent()) {
                        result += item.name();
                    }
                    if (Optional.ofNullable(item.arg()).isPresent()) {
                        result += " [" + item.arg() + "]";
                    }
                    if (Optional.ofNullable(item.description()).isPresent()) {
                        result += " - " + item.description();
                    }
                    System.out.println(result);
                });
    }

    @Override
    public String name() {
        return NAME;
    }

}
