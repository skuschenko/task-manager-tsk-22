package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.api.service.IAuthService;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "registry user";

    private final String NAME = "user-registry";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        final String password = TerminalUtil.nextLine();
        showParameterInfo("email");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @Override
    public String name() {
        return NAME;
    }

}
