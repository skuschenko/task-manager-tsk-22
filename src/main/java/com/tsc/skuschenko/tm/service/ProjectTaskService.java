package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final String PROJECT_ID = "project id";

    private final String TASK_ID = "task id";

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    public ProjectTaskService() {
    }

    public ProjectTaskService(
            final ITaskRepository taskRepository,
            final IProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Task bindTaskByProject(
            final String userId, final String projectId, final String taskId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        Optional.ofNullable(projectRepository.findOneById(userId, projectId))
                .orElseThrow(ProjectNotFoundException::new);
        final Task task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void clearProjects(final String userId) {
        final List<Project> projects = projectRepository.findAll(userId);
        Optional.ofNullable(projects).filter(item -> item.size() != 0).get()
                .forEach(item -> deleteProjectById(userId, item.getId()));
    }

    @Override
    public Project deleteProjectById(
            final String userId, final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        final List<Task> projectTasks =
                findAllTaskByProjectId(userId, projectId);
        Optional.ofNullable(projectTasks).filter(item -> item.size() != 0).get()
                .forEach(item -> taskRepository.remove(item));
        return projectRepository.removeOneById(userId, projectId);
    }

    @Override
    public List<Task> findAllTaskByProjectId(
            final String userId, final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(
            final String userId, final String projectId, final String taskId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        Optional.ofNullable(projectRepository.findOneById(userId, projectId))
                .orElseThrow(ProjectNotFoundException::new);
        final Task task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        return task;
    }

}
