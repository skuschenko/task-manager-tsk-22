package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> entityRepository;

    public AbstractService(final IRepository<E> entityRepository) {
        this.entityRepository = entityRepository;
    }

    @Override
    public void add(final E entity) {
        Optional.ofNullable(entity)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entityRepository.add(entity);
    }

    @Override
    public void clear() {
        entityRepository.clear();
    }

    @Override
    public List<E> findAll() {
        return entityRepository.findAll();
    }

    @Override
    public E findById(final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findById(id);
    }

    @Override
    public void remove(final E entity) {
        Optional.ofNullable(entity)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entityRepository.remove(entity);
    }

    @Override
    public E removeById(final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.removeById(id);
    }

}
