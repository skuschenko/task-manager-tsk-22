package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.*;
import com.tsc.skuschenko.tm.exception.entity.user.EmailExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.LoginExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(
            final String login, final String password, final String email
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        if (isEmailExist(email)) throw new EmailExistsException();
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = Optional.ofNullable(create(login, password))
                .orElseThrow(UserNotFoundException::new);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(
            final String login, final String password, final Role role
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = Optional.ofNullable(create(login, password))
                .orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (!Optional.ofNullable(email).isPresent()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public boolean isLoginExist(final String login) {
        if (!Optional.ofNullable(login).isPresent()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public User lockUserByLogin(String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        return userRepository.removeByLogin(login);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final User user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User unlockUserByLogin(String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        return user;
    }

    @Override
    public User updateUser(
            final String userId, final String firstName, final String lastName,
            final String middleName
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        final User user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
