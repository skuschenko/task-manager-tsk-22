package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IAuthService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyLoginException;
import com.tsc.skuschenko.tm.exception.empty.EmptyPasswordException;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void checkRoles(final Role... roles) {
        if (!Optional.ofNullable(roles)
                .filter(item -> item.length != 0)
                .isPresent()) return;
        final User user = getUser();
        Optional.ofNullable(user).orElseThrow(AccessDeniedException::new);
        final Role userRole = user.getRole();
        Optional.ofNullable(userRole).orElseThrow(AccessDeniedException::new);
        for (final Role role : roles) {
            if (role.getDisplayName().equals(userRole.getDisplayName())) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(final String login, final String password) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final User user = Optional.ofNullable(userService.findByLogin(login)).
                filter(item -> item.getPasswordHash()
                        .equals(HashUtil.salt(password)) && !item.isLocked())
                .orElseThrow(AccessDeniedException::new);
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(
            final String login, final String password, final String email
    ) {
        return userService.create(login, password, email);
    }

}
