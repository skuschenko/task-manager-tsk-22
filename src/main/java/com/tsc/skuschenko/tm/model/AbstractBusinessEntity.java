package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;

import java.io.Serializable;
import java.util.Date;

public class AbstractBusinessEntity extends AbstractEntity
        implements Serializable {

    private Date created = new Date();

    private Date dateFinish;

    private Date dateStart;

    private String description = "";

    private String name = "";

    private Status status = Status.NOT_STARTED;

    private String userId;

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getUserId() {
        if (userId == null || userId.isEmpty()) {
            throw new AccessDeniedException();
        }
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
