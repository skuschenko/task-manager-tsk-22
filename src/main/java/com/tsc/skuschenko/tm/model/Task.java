package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import com.tsc.skuschenko.tm.enumerated.Status;

public class Task extends AbstractBusinessEntity implements IWBS {

    private String projectId;

    private Status status = Status.NOT_STARTED;

    public Task() {
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

}
