package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task>
        implements ITaskRepository {

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        return findAll(userId).stream()
                .filter(item -> projectId.equals(item.getProjectId()))
                .collect(Collectors.toList());
    }

}
