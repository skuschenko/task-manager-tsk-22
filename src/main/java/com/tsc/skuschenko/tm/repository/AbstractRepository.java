package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity>
        implements IRepository<E> {

    protected List<E> entities = new ArrayList<>();

    @Override
    public void add(E entity) {
        entities.add(entity);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public E findById(final String id) {
        return entities.stream()
                .filter(item -> id.equals(item.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public E removeById(String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

}
