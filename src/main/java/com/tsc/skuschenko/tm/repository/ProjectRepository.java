package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.model.Project;

public class ProjectRepository extends AbstractBusinessRepository<Project>
        implements IProjectRepository {
}
