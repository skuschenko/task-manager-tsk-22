package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IBusinessRepository;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(String userId) {
        findAll(userId).forEach(entities::remove);
    }

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(
            final String userId, final Comparator<E> comparator
    ) {
        return entities.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findOneById(final String userId, final String id) {
        return entities.stream()
                .filter(item -> id.equals(item.getId()) &&
                        userId.equals(item.getUserId()))
                .findAny()
                .orElse(null);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        return entities.size() > index &&
                userId.equals(entities.get(index).getUserId()) ?
                entities.get(index) : null;
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        return entities.stream()
                .filter(item -> name.equals(item.getId()) &&
                        userId.equals(item.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findOneById(userId, id));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        final Optional<E> entity =
                Optional.ofNullable(findOneByIndex(userId, index));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        final Optional<E> entity
                = Optional.ofNullable(findOneByName(userId, name));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

}
